package com.apache.camel.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apache.camel.domain.Department;
import com.apache.camel.repository.JdbcDepartmentRepository;

@Service
public class DepartmenService {
  @Autowired
  JdbcDepartmentRepository jdbcDepartmentRepository;

  public int saveDepartment(Department department) {
    return jdbcDepartmentRepository.save(department);
  }

  public List<Department> findAll() {
    return jdbcDepartmentRepository.findAll();
  }

  public int updateDepartment(Department department) {
    return jdbcDepartmentRepository.update(department);
  }

  public Optional<Department> findById(Long id) {
    return jdbcDepartmentRepository.findById(id);
  }
}
