package com.apache.camel.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.apache.camel.domain.Employee;
import com.apache.camel.repository.JdbcEmployeeRepository;

@Service
public class EmployeeService {
  @Autowired
  JdbcEmployeeRepository jdbcEmployeeRepository;

  public int saveEmployee(Employee employee) {
    return jdbcEmployeeRepository.save(employee);
  }

  public int updateEmployee(Employee employee) {
    return jdbcEmployeeRepository.update(employee);
  }

  public List<Employee> findAll() {
    return jdbcEmployeeRepository.findAll();
  }

  public Optional<Employee> findById(Long id) {
    return jdbcEmployeeRepository.findById(id);
  }
}
