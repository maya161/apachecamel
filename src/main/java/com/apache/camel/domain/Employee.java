package com.apache.camel.domain;

import lombok.*;

@Setter
@Getter
@ToString
public class Employee {
  private Long id;
  private String name;
  private Long department_id;
  // private Department department;

  public Employee(Long id, String name, Long departmentId) {
    this.id = id;
    this.name = name;
    this.department_id = departmentId;
  }

  public Employee(String name, Long departmentId) {
    this.name = name;
    this.department_id = departmentId;
  }
}
