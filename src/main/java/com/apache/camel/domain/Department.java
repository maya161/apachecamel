package com.apache.camel.domain;

import lombok.*;

@Setter
@Getter
public class Department {
  private Long id;
  private String name;

  public Department(Long id, String name) {
    this.id = id;
    this.name = name;
  }
}
