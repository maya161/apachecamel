package com.apache.camel.repository;

import java.util.List;
import java.util.Optional;

import com.apache.camel.domain.Employee;

public interface EmployeeRepository {
  int save(Employee employee);

  int update(Employee employee);

  Optional<Employee> findById(Long id);

  List<Employee> findAll();

}
