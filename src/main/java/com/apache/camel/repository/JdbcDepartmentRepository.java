package com.apache.camel.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.apache.camel.domain.Department;
import com.apache.camel.mapper.DeparmentMapper;

@Repository
public class JdbcDepartmentRepository implements DepartmentRepository {
  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Override
  public int save(Department department) {
    String sql = "INSERT INTO department (name) values(?)";
    return jdbcTemplate.update(sql, new Object[] { department.getName() });
  }

  @Override
  public List<Department> findAll() {
    String sql = "SELECT * FROM DEPARTMENT;";
    return jdbcTemplate.query(sql, new DeparmentMapper());
  }

  @Override
  public int update(Department department) {
    String sql = "UPDATE department SET name=? WHERE id=? ;";
    return jdbcTemplate.update(sql, department.getName(), department.getId());
  }

  @Override
  public Optional<Department> findById(Long id) {
    String sql = "SELECT * FROM DEPARTMENT WHERE id=?;";
    return jdbcTemplate.query(sql, new DeparmentMapper(), id).stream().findFirst();
  }
}