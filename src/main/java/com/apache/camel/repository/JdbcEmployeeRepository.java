package com.apache.camel.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.apache.camel.domain.Employee;
import com.apache.camel.mapper.EmployeeMapper;

@Repository
public class JdbcEmployeeRepository implements EmployeeRepository {

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Override
  public int save(Employee employee) {
    String sql = "INSERT INTO employee (name, department_id) values(?,?) ";
    return jdbcTemplate.update(sql, new Object[] { employee.getName(), employee.getDepartment_id() });
  }

  @Override
  public int update(Employee employee) {
    String sql = "UPDATE employee SET name=?, department_id=? where id = ? ";
    return jdbcTemplate.update(sql, new Object[] { employee.getName(), employee.getDepartment_id() });
  }

  @Override
  public List<Employee> findAll() {
    String sql = "select * from employee;";
    return jdbcTemplate.query(sql, new EmployeeMapper());
  }

  @Override
  public Optional<Employee> findById(Long id) {
    String sql = "select * from employee where id = ?";
    return jdbcTemplate.query(sql, new EmployeeMapper(), id).stream().findFirst();
  }
}
