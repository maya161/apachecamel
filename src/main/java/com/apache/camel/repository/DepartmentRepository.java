package com.apache.camel.repository;

import java.util.List;
import java.util.Optional;

import com.apache.camel.domain.Department;

public interface DepartmentRepository {

  int save(Department department);

  int update(Department department);

  Optional<Department> findById(Long id);

  List<Department> findAll();

}
