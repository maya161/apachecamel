package com.apache.camel.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.apache.camel.domain.Employee;
import com.apache.camel.service.EmployeeService;

@RestController
@RequestMapping("/api")
public class EmployeeController {
  @Autowired
  EmployeeService employeeService;

  @PostMapping("/employee")
  public String saveEmployee(@RequestBody Employee employee) {
    int value = employeeService.saveEmployee(employee);
    if (value == 1) {
      return "Record inserted succesfully";
    }
    return "Problem while inserting record";
  }

  @PutMapping("/employee")
  public String updateEmployee(@RequestBody Employee employee) {
    int value = employeeService.updateEmployee(employee);
    if (value == 1) {
      return "Record updated succesfully";
    }
    return "Problem while updating record";
  }

  @GetMapping("/employee")
  public List<Employee> findAll() {
    return employeeService.findAll();
  }

  @GetMapping("/employee/{id}")
  public Optional<Employee> findById(@PathVariable Long id) {
    return employeeService.findById(id);
  }
}
