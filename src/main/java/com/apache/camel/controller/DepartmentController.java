package com.apache.camel.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.apache.camel.domain.Department;
import com.apache.camel.service.DepartmenService;

@RestController
@RequestMapping("/api")
public class DepartmentController {
  @Autowired
  DepartmenService departmenService;

  @PostMapping("/department")
  public String saveDepartment(@RequestBody Department department) {
    int value = departmenService.saveDepartment(department);
    if (value == 1) {
      return "Record inserted succesfully";
    }
    return "Problem while inserting record";
  }

  @PutMapping("/department")
  public String updateDepartment(@RequestBody Department department) {
    int value = departmenService.updateDepartment(department);
    if (value == 1) {
      return "Record updated succesfully";
    }
    return "Problem while updating record";
  }

  @GetMapping("/department")
  public List<Department> findAll() {
    return departmenService.findAll();
  }

  @GetMapping("/department/{id}")
  public Optional<Department> findById(@PathVariable Long id) {
    return departmenService.findById(id);
  }
}
