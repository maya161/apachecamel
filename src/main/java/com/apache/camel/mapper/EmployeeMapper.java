package com.apache.camel.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import com.apache.camel.domain.Employee;
import com.apache.camel.domain.Department;

public class EmployeeMapper implements RowMapper<Employee> {
  @Autowired
  Department department;

  @Override
  public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
    return new Employee(
        rs.getLong("id"),
        rs.getString("name"),
        rs.getLong("department_id"));
  }
}
