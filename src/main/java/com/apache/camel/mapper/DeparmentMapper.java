package com.apache.camel.mapper;

import com.apache.camel.domain.Department;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DeparmentMapper implements RowMapper<Department> {

  @Override
  public Department mapRow(ResultSet rs, int rowNum) throws SQLException {
    return new Department(
        rs.getLong("id"),
        rs.getString("name"));
  }
}
