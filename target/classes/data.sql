insert into department(id, name) values(1, 'Sistemas');
insert into department(id, name) values(2, 'Contabilidad');
insert into department(id, name) values(3, 'Auditoria');
insert into department(id, name) values(4, 'Recursos Humanos');
insert into department(id, name) values(5, 'Mantenimiento');

insert into employee(id, name, department_id) values(1, 'Ascari Romo', 1);
insert into employee(id, name, department_id) values(2, 'Juan Zavala', 2);
insert into employee(id, name, department_id) values(3, 'Antonio Pérez', 3);
insert into employee(id, name, department_id) values(4, 'Marco Ávila', 3);
insert into employee(id, name, department_id) values(5, 'Pedro Martínez', 1);